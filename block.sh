#!/bin/bash

# 定义要屏蔽的端口列表
ports="22,25,26,465,587,24,50,57,105,106,158,209,1109,6969,6881-6889"

# 检查是否以root权限运行
if [[ $EUID -ne 0 ]]; then
   echo "请以root权限运行此脚本." 
   exit 1
fi

# 清空已有的nftables规则
nft flush ruleset

# 添加表
nft add table inet filter

# 添加链
nft add chain inet filter output { type filter hook output priority 0 \; }

# 对指定端口的出向流量进行屏蔽
for port in $(echo $ports | sed "s/,/ /g")
do
  nft add rule inet filter output tcp dport $port counter drop
  nft add rule inet filter output udp dport $port counter drop
done

# 保存规则到文件
nft list ruleset > /etc/nftables.conf

echo "端口屏蔽设置完成。已屏蔽端口: $ports"